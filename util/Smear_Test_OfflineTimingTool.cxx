//___________________________________________
//
// Example macro to test MC Smearing 
//
//  Author: Ryne Carbone
//  Date:   March 2016
//  Email:  rc2809@columbia.edu
//          ryne.carbone@cern.ch
//          Updated by Jue Chen since Oct. 2016
// Updates since Oct 2016:   Jue Chen
// Email:         jue@cern.ch
//
//  Information: The tool requires an Online
//    Cell ID, a raw time, an energy, PV_z and
//    an electronic gain. It then smears the
//    MC time to match the observed resolution
//    in the LAr Calorimeter
//
//  User Input:
//    1. Make an object
//       $ OfflineTimingTool m_ott;
//    2. If you have multiple particles, create a
//       variable to store the collision time, since
//       it should be correlated for particles from
//       the same PV. Make a boolean flag which tells
//       the function to correlate the collision time
//       or not.
//    3. Find the z position of the Primary Vertex (PV_z)
//    4. For the first particle send the el/ph, PV_z, 
//       collision time variable, and false boolean. 
//       $ double t_smear = m_ott.getSmearedTime<const xAOD::Electron>(*el, PV_z, t_coll, false);
//    5. For the second particle send the el/ph, PV_z,
//       collision time variable, and true boolean
//       $ double t_corr_smear = m_ott.getSmearedTime<const xAOD::Electron>(*el, PV_z, t_coll, true);
//
//  This Example:
//    -> This example will read from an xAOD and
//       loop throught the events. It creates an 
//       empty collision time variable and sends it
//       with an electron, primary vertex z position,
//       and boolean called correlate to the 
//       Offline Timing Tool to get a smeared time value.
//    -> The 'getSmearedTime' function reads the 
//       raw time for the first particle, corrects for TOF
//       and creates a smeared time to match 
//       the measured resolution
//    -> The collision time variable is used by the 
//       function 'getSmearedTime' to correlate
//       the times for multiple particles coming
//       from the same PV if the flag is set to true
//    -> There are a few histograms produced
//       to illustrate the effect of the
//       smearing (out_sampleSmearHist.root)
//
//___________________________________________

// General/ROOT includes
#include <TSystem.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TF1.h>
#include <TCanvas.h> 
#include <TStyle.h> 
#include <TProfile.h>
#include <TLatex.h>
#include <iostream>

// Include Tool
#include "OfflineTimingTool/OfflineTimingTool.h"

// Root Access Includes
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/tools/Message.h"

// Event Info Includes
#include "xAODEventInfo/EventInfo.h"

// Egamma Includes
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/PhotonAuxContainer.h"

// Vertex Includes
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/VertexAuxContainer.h"
#include "xAODTracking/Vertex.h"


// Make summary canvas
void  makeSummary (TH2F* h1, TH2F* h2, std::string sname, std::string lname, std::string dname);
void  makeSummary (TH1F* h1, TH1F* h2, std::string sname, std::string lname);
void  makeCorrPlot(TH2F* h1, std::string sname, std::string lname);
void  makeResPlot (TH2F* h1, TH2F* h2, std::string sname, std::string lname, std::string dname);
void  makeBeamSpread(TH1F* h1, std::string sname, std::string lname);
// Make plots look nicer
TStyle setStyle();
void   myText(Double_t x, Double_t y, Color_t color, char *text, double scale);
// TOF for some initial plots
double applyTOF(double x, double y, double z, double t, double PV_z);
// For analyzing plots
TH2F   rebin       (TH2F *h, int entPerBin, int firstBin);
TH1F*  fitGaus(TH1F* h_cell, double mean, double rms);
// Labels for plots
const std::string gain[3] = {"High","Medium","Low"};
const std::string det[2] = {"EMB","EMEC"};

//
// Loop over sample xAOD
// Apply correction to time
// Plot variables before/after
// _______________________________
int main( int argc, char* argv[] ) {

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  if( argc > 1 ) submitDir = argv[ 1 ];
  
  // Set the style
  static TStyle myStyle = setStyle();
  myStyle.cd();

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Open an example xAOD
  const std::string fileName = PathResolverFindCalibFile("/afs/cern.ch/work/r/rcarbone/public/OfflineTimingTool/sample_MC_xAOD.root");
  std::unique_ptr< TFile > f( TFile::Open( fileName.data(), "READ" ) );
  
  // Make Sure File Opened Correctly
  if( f.get() ) Info( "Smear_Test_OfflineTimingTool", Form("Opened sample xAOD successfully") );
  else{ Info( "Smear_Test_OfflineTimingTool", Form("Failed to open sample xAOD") );return -1; }
 
  // Create output File
  TFile* outFile = new TFile("out_sampleSmearHist.root","RECREATE");

  // Create histograms to look at results
  TH2F* h_e_ti[3][2]; // [0-1][]high/med/lo gain; [][0-1] emb-emec
  TH2F* h_e_tf[3][2];
  TH1F* h_t_in    = new TH1F("t_in","Initial Time; Initial Time[ns]; Events / 0.1 ns",100,-5,5);
  TH1F* h_t_out   = new TH1F("t_out","Final Time; Final Time[ns]; Events / 0.1 ns",100,-5,5);
  TH2F* h_t1_t2_i = new TH2F("t1_t2_i","Initial t_{1} vs t_{2}; Leading Electron Time [ns]; Subleading Electron Time [ns]",100,-5,5,100,-5,5);
  TH2F* h_t1_t2_f = new TH2F("t1_t2_f","Final t_{1} vs t_{2}; Leading Electron Time [ns]; Subleading Electron Time [ns]",100,-5,5,100,-5,5);
  TH1F* h_t1pt2   = new TH1F("t1_plus_t2","Leading Plus Subleading Electron Time; t_{1} + t_{2} [ns]; Events / 0.1 ns",100,-5,5);
  TH1F* h_t1mt2   = new TH1F("t1_minus_t2","Leading Minus Subleading Electron Time; t_{1} - t_{2} [ns]; Events / 0.04 ns",250,-5,5);
  for( int g=0; g<3; g++){
    for( int d=0; d<2; d++){
      h_e_ti[g][d] = new TH2F(Form("e_ti_%s%s",det[d].data(), gain[g].data()),"Energy vs Initial Time; Energy [GeV];Initial Time [ns]", 100, 0, 250, 100, -5, 5);
      h_e_tf[g][d] = new TH2F(Form("e_tf_%s%s",det[d].data(), gain[g].data()),"Energy vs Final Time; Energy[GeV]; Final Time[ns]", 100, 0, 250, 100, -5, 5);
    }
  }
  
  // Create TEvent
  xAOD::TEvent event( static_cast<TFile*>(nullptr), xAOD::TEvent::kClassAccess);
  if( event.readFrom(f.get()) != xAOD::TReturnCode::kSuccess ){
    Info( "Smear_Test_OfflineTimingTool","Failed to read TEvent!"); 
    return -1;
  }
  
  // Find max number of entries to loop on
  Info( "Smear_Test_OfflineTimingTool", "Number of events in the file: %llu", event.getEntries() );
  Long64_t nentries = event.getEntries();
 
  // Create Offline Timing Tool object to test (optionally send debug option)
  //bool debug = true;
  //nentries = 1;
  //OfflineTimingTool ott(debug);
  OfflineTimingTool ott;

  // Loop over the events
  for( Long64_t entry = 0; entry < nentries; ++entry){
    
    // Get the correct entry
    event.getEntry(entry);
    if( !(entry%1000) ) 
      Info( "Smear_Test_OfflineTimingTool", Form("Event: %llu",entry) );

    // Get Electrons
    const xAOD::ElectronContainer* electrons = 0;
    if( event.retrieve( electrons, "Electrons" ) != xAOD::TReturnCode::kSuccess){
      Info( "Smear_Test_OfflineTimingTool","Failed to retreive Electron Container!"); 
      return -1;
    }
   
    // Get PV container
    const xAOD::VertexContainer* primVtxCont = 0;
    if( event.retrieve( primVtxCont, "PrimaryVertices") != xAOD::TReturnCode::kSuccess){
      Info( "LocalTest_OfflineTimingTool","Failed to retreive PrimaryVertices Container!");
      return -1;
    }
    xAOD::VertexContainer::const_iterator ver_itr = primVtxCont->begin();
    xAOD::VertexContainer::const_iterator ver_end = primVtxCont->end();
    // Find PV_z
    float PV_z = -9999;
    for ( ; ver_itr != ver_end; ver_itr++){
      if( (*ver_itr)->vertexType() != xAOD::VxType::PriVtx) continue; // Find the PV
      if( (*ver_itr)->nTrackParticles() < 3) continue; // need 3 associated tracks
      PV_z = (*ver_itr)->z();
    }

    // Make sure we got a valid PV_z
    if( PV_z == -9999 ) continue;

    // Pass this variable to 'getSmearedTime' so the application
    // can correlate collision times for particles from the same PV
    double t_coll = -999;      // used to correlate multiple electron times
    
    // For plotting correlation between leading/subleading times
    double t_raw1, t_raw2;     // two electron raw times
    double t_smear1, t_smear2; // two electron smear times
    double e_1, e_2;           // two electron energies
    int el_num = 0;            // event electron counter

    // Loop over electrons
    for( const xAOD::Electron *el : *electrons ){
      
      // Only layer 2 electrons!
      double eta = el->caloCluster()->eta();
      if( fabs(eta) > 2.47 || (fabs(eta) > 1.37 && fabs(eta) < 1.52) )
        continue;
      
      // At least 5 GeV!
      double el_e = el->auxdata<float>("maxEcell_energy")/1000.;
      if( el_e < 5. )
        continue;
      
      // High/Med gain (low is okay too)
      int el_gain = el->auxdata<int>("maxEcell_gain");
      
      el_num++;
      
      // Fill some initial plots
      float el_t = el->auxdata<float>("maxEcell_time");
      float x    = el->auxdata<float>("maxEcell_x");
      float y    = el->auxdata<float>("maxEcell_y");
      float z    = el->auxdata<float>("maxEcell_z");
      // apply TOF (just for plotting initial times in this macro, 
      // the tool does this automatically in the smeared time)
      el_t = applyTOF( x, y, z, el_t, PV_z); 
      h_t_in->Fill(el_t);
      if( fabs(eta) < 1.45 ) // EMB
        h_e_ti[el_gain][0]->Fill(el_e, el_t);
      else                   // EMEC
        h_e_ti[el_gain][1]->Fill(el_e, el_t);
     
      // Get the smeared time
      double el_t_smear; 
      bool correlate = false;
      if( el_num == 1){  // first electron
        el_t_smear = ott.getSmearedTime<const xAOD::Electron>(*el, PV_z, t_coll, correlate);
      }
      else{              // after use correlated collision time
        correlate = true;
        el_t_smear = ott.getSmearedTime<const xAOD::Electron>(*el, PV_z, t_coll, correlate);
      }
      
      // Save leading/subleading electron info
      if( el_num == 1 ){
        t_smear1 = el_t_smear;
        t_raw1   = el_t;
        e_1      = el_e;
      }else if( el_num == 2){
        t_smear2 = el_t_smear;
        t_raw2   = el_t;
        e_2      = el_e;
      }
     
      // Fill some final plots
      h_t_out->Fill(el_t_smear);
      if( fabs(eta) < 1.45) // EMB
        h_e_tf[el_gain][0]->Fill(el_e, el_t_smear);
      else                  // EMEC
        h_e_tf[el_gain][1]->Fill(el_e, el_t_smear);
      
      // Print initial/final time
      if( !(entry%1000) ) 
        Info("Smear_Test_OfflineTimingTool", Form("     Initial time: %f;\tFinal (smeared) time: %f", el_t, el_t_smear) );
    
    }// end loop over electrons

    // Fill the leading/subleading plots
    if( el_num != 2 ) continue; // need exactly two
    if( e_1 > e_2 ){
      h_t1_t2_i->Fill( t_raw1, t_raw2 );
      h_t1_t2_f->Fill( t_smear1, t_smear2 );
      if( e_1 > 20 && e_2 > 20){
        h_t1pt2->Fill(t_smear1+t_smear2);
        h_t1mt2->Fill(t_smear1-t_smear2);
      }
    }else{
      h_t1_t2_i->Fill( t_raw2, t_raw1 );
      h_t1_t2_f->Fill( t_smear2, t_smear1 );     
      if( e_1 > 20  && e_2 > 20 ){
        h_t1pt2->Fill(t_smear2+t_smear1);
        h_t1mt2->Fill(t_smear2-t_smear1);
      }
    }

  }// end loop over events

  // Make summary plots for avg time
  makeSummary(h_t_in, h_t_out, "time", "Time Before & After Smearing");
  for( int d=0; d<2; d++){
    TH2F* hi = (TH2F*)h_e_ti[0][d]->Clone("");
    TH2F* hf = (TH2F*)h_e_tf[0][d]->Clone("");
    hi->Add(h_e_ti[1][d]);
    hf->Add(h_e_tf[1][d]);
    makeSummary(hi, hf, Form("energy_%s",det[d].data()), "Energy vs Time Before & After Smearing", det[d].data());
  }
  
  // Make leading/subleading correlation plots
  makeCorrPlot(h_t1_t2_i, "corr_i", "Initial Leading vs Subleading Electron Times");
  makeCorrPlot(h_t1_t2_f, "corr_f", "Final Leading vs Subleading Electron Times");
  
  // Make resolution Plots
  for( int d=0; d<2; d++){
    makeResPlot(h_e_ti[0][d],h_e_ti[1][d], Form("res_i_%s",det[d].data()), Form("Initial Time Resolution %s", det[d].data()), det[d].data());
    makeResPlot(h_e_tf[0][d],h_e_tf[1][d], Form("res_f_%s",det[d].data()), Form("Final Time Resolution %s", det[d].data()), det[d].data());
  }  
  
  // Make beamspread plots
  h_t1mt2->GetXaxis()->SetRangeUser(-2,2);
  makeBeamSpread(h_t1pt2, "t1_plus_t2", "Leading Plus Subleading Electron Time");
  makeBeamSpread(h_t1mt2, "t1_minus_t2", "Leading Minus Subleading Electron Time");

  // Save histograms to file 
  outFile->Write();
  Info("Smear_Test_OfflineTimingTool","Saved summary histograms to out_sampleSmearHist.root");
  
  // Close File
  outFile->Close();

  return 0;
}


// 
// Make t1/t2 plots
//___________________________________________________________________
void makeBeamSpread(TH1F* h1, std::string sname, std::string lname){
  
  // Before/After
  TCanvas* c1 = new TCanvas(Form("c_%s",sname.data()), Form("%s",lname.data()), 800,700);
  h1->SetTitle(lname.data());
  h1->Draw();
  h1 = fitGaus( h1, h1->GetMean(), h1->GetRMS() );
  h1->GetYaxis()->SetRangeUser(0.5,h1->GetMaximum()*8);
  c1->SetLogy();

  // Put Mean/RMS time on plot
  myText(0.20, 0.88, kBlack, Form("MC"), 1.0);
  myText(0.65, 0.83, kBlack, Form("Mean: %.3f ns",h1->GetMean()), 0.6 );
  myText(0.65, 0.78, kBlack, Form("RMS:  %.3f ns",h1->GetRMS()), 0.6 );

  c1->Write();
  delete c1;

}


//
// Plot resolution vs energy
//______________________________________________________________
void makeResPlot(TH2F* h1, TH2F* h2, std::string sname, std::string lname, std::string dname){

  TCanvas *c1 = new TCanvas(Form("c_%s",sname.data()), Form("%s",lname.data()), 800, 700);
  // rebin histogram
  (*h1) = rebin( h1, h1->GetEntries()/20, 2); // roughly ~1/20 entries per bin, 5GeV cut so first 2 bins empty
  (*h2) = rebin( h2, h2->GetEntries()/10, 2);
  
  // Find fit range
  double enLow[2]; 
  double enHigh[2];
  double lowBin[2];
  double highBin[2];
  lowBin[0]  = h1->GetXaxis()->FindBin(6.);
  lowBin[1]  = h2->GetXaxis()->FindBin(6.) + 1;
  highBin[0] = h1->GetXaxis()->FindBin(249.) - 1;
  highBin[1] = h2->GetXaxis()->FindBin(249.);
  enLow[0]   = h1->GetXaxis()->GetBinCenter( lowBin[0] );
  enLow[1]   = h2->GetXaxis()->GetBinCenter( lowBin[1] );
  enHigh[0]  = h1->GetXaxis()->GetBinCenter( highBin[0] );
  enHigh[1]  = h2->GetXaxis()->GetBinCenter( highBin[1] );

  // Find resolution per energy bin
  TObjArray aSlices;
  TObjArray bSlices;
  h1->FitSlicesY(0, lowBin[0], highBin[0], 2, "QNR", &aSlices); // 0: gaus
  h2->FitSlicesY(0, lowBin[1], highBin[1], 2, "QNR", &bSlices);

  // Make resolution histogram (copy binning structure)
  TH1F h_res[2];
  h_res[0]  = TH1F(Form("1d_h_%s",sname.data()),
                       Form("1d_h_%s",lname.data()),
                       h1->GetNbinsX(),
                       h1->GetXaxis()->GetXbins()->GetArray() );
  h_res[1]  = TH1F(Form("1d_m_%s",sname.data()),
                       Form("1d_m_%s",lname.data()),
                       h2->GetNbinsX(),
                       h2->GetXaxis()->GetXbins()->GetArray() );
  h_res[0] = (TH1F&)*(aSlices[2]); // [2] is sigma of gaussian fit
  h_res[1] = (TH1F&)*(bSlices[2]); 
  
  // Fit the resolution plot to assumed form
  TF1 f1("f1","sqrt( sq([0]/x)+sq([1]))", enLow[0], enHigh[0]);
  TF1 f2("f2","sqrt( sq([0]/x)+sq([1]))", enLow[1], enHigh[1]);
  f1.SetLineColor(kRed);
  f2.SetLineColor(kRed);
  h_res[0].Fit("f1","QBR");
  h_res[1].Fit("f2","QBR+");

  // Make plot
  h_res[0].SetTitle(lname.data());
  h_res[0].Draw();
  h_res[1].SetLineColor(kBlue);
  h_res[1].SetMarkerColor(kBlue);
  h_res[1].Draw("same");
  h_res[0].GetXaxis()->SetTitle("Energy [GeV]");
  h_res[0].GetYaxis()->SetTitle("Time Resolution [ns]");
  h_res[0].GetXaxis()->SetRangeUser(4,250.);
  h_res[0].GetYaxis()->SetRangeUser(0.0,0.6);
  c1->SetLogx(1);

  // Put info on plot 
  myText(0.20, 0.88, kBlack, Form("MC"), 1.0);
  myText(0.20, 0.82, kBlack, Form("%s",dname.data()), .7);
  myText(0.40, 0.74, kBlack, Form("#sigma( t ) = #frac{p_{0}}{E} #oplus p_{1}"), 0.8);
  myText(0.73, 0.78, kBlack, Form("Fit Results:"), .7 );
  myText(0.67, 0.74, kBlack, Form("High"), 0.7);
  myText(0.73, 0.74, kRed,   Form("p_{0}: %.3f", f1.GetParameter(0)), .7 );
  myText(0.73, 0.70, kRed,   Form("p_{1}: %.3f", f1.GetParameter(1)), .7 );
  myText(0.63, 0.66, kBlue,  Form("Medium"), 0.7);
  myText(0.73, 0.66, kRed,   Form("p_{0}: %.3f", f2.GetParameter(0)), .7 );
  myText(0.73, 0.62, kRed,   Form("p_{1}: %.3f", f2.GetParameter(1)), .7 );
  
  c1->Write();
  delete c1;


}


//
// Take 2D hist of t1 vs t2 and show correlation
// between leading and subleading electrons
//___________________________________________________________________________
void makeCorrPlot(TH2F* h1, std::string sname, std::string lname){
  
  TCanvas *c1 = new TCanvas( Form("c_%s",sname.data()), Form("%s",lname.data()), 900, 750 );
  
  // Set style
  c1->SetRightMargin(0.17);
  gStyle->SetPalette(1,0);
  
  // Draw
  // h1->Rebin2D(5,5);
  h1->SetTitle(lname.data());
  h1->GetXaxis()->SetRangeUser(-3.,3.);
  h1->GetYaxis()->SetRangeUser(-3.,3.);
  h1->Draw("colz");
  
  // Put info on plot
  int    ent      = h1->GetEntries();
  double corr     = h1->GetCorrelationFactor(1,2);
  double corr_err = sqrt( (1-corr*corr)/(ent-2) ); 
  double meanX    = h1->GetMean(1);
  double meanY    = h1->GetMean(2);
  double rmsX     = h1->GetRMS(1);
  double rmsY     = h1->GetRMS(2);
  myText(0.20, 0.88, kBlack, Form("MC"), 1.0);
  myText(0.20, 0.83, kBlack, Form("Corr(x,y) =%.3f #pm %.3f", corr, corr_err), .8 );
  myText(0.63, 0.83, kBlack, Form("Entries: %d",ent), .6 );
  myText(0.63, 0.80, kBlack, Form("Mean X: %.3f ns",meanX), .6 );
  myText(0.63, 0.77, kBlack, Form("RMS X: %.3f ns",rmsX), .6 );
  myText(0.63, 0.74, kBlack, Form("Mean Y: %.3f ns",meanY), .6 );
  myText(0.63, 0.71, kBlack, Form("RMS Y: %.3f ns",rmsY), .6 );

  c1->Write();
  delete c1;

}


//
// Take 2D hist before/after time correction
// Compare distributions, plot mean/rms
//_______________________________________________________________________
void makeSummary(TH2F* hi, TH2F* hf, std::string sname, std::string lname, std::string dname){

  // TProfile Before/After
  TCanvas* c1 = new TCanvas(Form("c_%s",sname.data()), Form("%s",lname.data()), 800,700);
  TProfile tp1 = *(hi->ProfileX());
  TProfile tp2 = *(hf->ProfileX());
  tp1.SetTitle(lname.data()); // Put title on Plot
  tp1.GetYaxis()->SetRangeUser(-0.5, 0.5);
  tp1.GetYaxis()->SetTitle("Time [ns]");
  tp1.Rebin(5);
  tp2.Rebin(5);
  tp1.Draw();
  tp2.SetLineColor(kRed);
  tp2.SetMarkerColor(kRed);
  tp2.Draw("same");

  // Put Mean/RMS time on plot
  myText(0.20, 0.88, kBlack, Form("MC"), 1.0);
  myText(0.20, 0.82, kBlack, Form("%s",dname.data()), .7);
  myText(0.65, 0.85, kBlack, Form("t(initial) Mean: %.3f ns",hi->GetMean(2)), 0.6 );
  myText(0.65, 0.80, kBlack, Form("t(initial) RMS:  %.3f ns",hi->GetRMS(2)), 0.6 );
  myText(0.65, 0.75, kRed,   Form("t(final) Mean: %.3f ns",hf->GetMean(2)), 0.6 );
  myText(0.65, 0.70, kRed,   Form("t(final) RMS:  %.3f ns",hf->GetRMS(2)), 0.6 );

  c1->Write(); 
  delete c1;
}


//
// Overloaded to accept TH1F
//______________________________________________________________________
void makeSummary(TH1F* hi, TH1F* hf, std::string sname, std::string lname){
  
  // Before/After
  TCanvas* c1 = new TCanvas(Form("c_%s",sname.data()), Form("%s",lname.data()), 800,700);
  hi->SetTitle(lname.data());
  hi->Draw();
  hi->GetYaxis()->SetRangeUser(0.5,hf->GetMaximum()*8);
  hf->SetLineColor(kRed);
  hf->Draw("same");
  c1->SetLogy();

  // Put Mean/RMS time on plot
  myText(0.20, 0.88, kBlack, Form("MC"), 1.0);
  myText(0.65, 0.85, kBlack, Form("t(initial) Mean: %.3f ns",hi->GetMean()), 0.6 );
  myText(0.65, 0.80, kBlack, Form("t(initial) RMS:  %.3f ns",hi->GetRMS()), 0.6 );
  myText(0.65, 0.75, kRed,   Form("t(final) Mean: %.3f ns",hf->GetMean()), 0.6 );
  myText(0.65, 0.70, kRed,   Form("t(final) RMS:  %.3f ns",hf->GetRMS()), 0.6 );
  
  c1->Write(); 
  delete c1;
}

//
// Apply TOF to MC to remove initial correlation
//_________________________________________________________________
double applyTOF(double x, double y, double z, double t, double PV_z){

  // Speed of light in mm/ns
  const double c_mmns = 299.792458;

  // Distances for cell and Cell minus PV_z
  double r_cell = sqrt( x*x + y*y + z*z);
  double r_path = sqrt( x*x + y*y + (z - PV_z)*(z - PV_z));

  // TOF correction
  double t_tof = (r_path - r_cell)/c_mmns;

  // Save correction
  double corrected_time =  t-t_tof;
  return corrected_time;
}


//
// Rebin the 2D histogram so that each bin has at
// least entPerBin entires
// firstBin is the first bin you want to consider
//________________________________________________
TH2F rebin(TH2F *h, int entPerBin, int firstBin) {

  // hold the bin edge information
  std::vector< double > xbins;
  // Combine all bins below first bin (no data below 5Gev so these will be empty)
  xbins.push_back( h->GetXaxis()->GetBinLowEdge(1) );
  xbins.push_back( h->GetXaxis()->GetBinLowEdge(firstBin + 1) ); 

  // keep track of last bin with at least entPerBin
  int lastFullIndex = firstBin; 

  // Get the xaxis
  TAxis *axis = h->GetXaxis();

  // Loop over bins in xaxis, start after first bin
  for (int i = (firstBin + 1); i <= h->GetNbinsX() - firstBin; i++) {

    // Get entries in this bin, and width
    int    y = h->Integral(i,i);
    double w = axis->GetBinWidth(i);

    // If not enough entries, need to combine bins
    if (y <= entPerBin){
      // Find integral from last combined bin
      double integral = h->Integral(lastFullIndex+1, i);
      if (integral <= entPerBin ) continue;
      // if above threshold, mark as new bin
      lastFullIndex = i;
      xbins.push_back( axis->GetBinLowEdge(i) + w);
    }
    else{
      // above threshold, mark as bin
      lastFullIndex = i;
      xbins.push_back( axis->GetBinLowEdge(i) + w );
    }

  }

  // put bin edges into an array
  xbins.push_back( axis->GetXmax() );
  size_t s = xbins.size();
  double *xbinsFinal = &xbins[0];

  // create new histo with new bin edges
  TH2F h_return = TH2F ( Form("hrebin_%s",h->GetName()), 
                         h->GetTitle(), 
                         s-1, xbinsFinal, 
                         h->GetNbinsY(), -5, 5);
  h_return.GetXaxis()->SetTitle( h->GetXaxis()->GetTitle() );

  // fill new histo with old values
  for( int i=1; i<=h->GetNbinsX(); i++){
    for( int j=1; j<=h->GetNbinsY(); j++){
      h_return.Fill( h->GetXaxis()->GetBinCenter(i), 
                     h->GetYaxis()->GetBinCenter(j), 
                     h->GetBinContent(i,j));
    }
  }
  
  return h_return;
}

//_____________________________________________
//
// Make a gaussian fit for sub range,
// Show dotted line for full range,
// put fit params
//_____________________________________________
TH1F* fitGaus(TH1F* h_cell, double mean, double rms){

  // Hide fit stats
  gStyle->SetOptFit(0);

  // Gaus fit (limited range around peak)
  TF1 fg("fg","gaus",mean-2*rms, mean+2*rms);

  // guess starting point for fit
  fg.SetParameter(1,mean);
  fg.SetParameter(2,rms);

  // put limits on the paramters
  fg.SetParLimits(1,mean-0.5*rms, mean+0.5*rms);
  fg.SetParLimits(2,0.5*rms,2*rms);

  // Draw the fit in subrange
  fg.SetLineColor(kRed);
  h_cell->Fit("fg","QBR");

  // Make the fit extend and draw as dotted line
  TF1 f2("f2","gaus",-10,10);
  f2.FixParameter( 0, fg.GetParameter(0) );
  f2.FixParameter( 1, fg.GetParameter(1) );
  f2.FixParameter( 2, fg.GetParameter(2) );
  f2.SetLineColor(kRed);
  f2.SetLineStyle(7); //dashed line
  h_cell->Fit("f2","QBR+");

  // Put parameters on the plot
  double pos_x = 0.65;
  myText(pos_x, 0.72,kBlack,Form("#mu: %.3f #pm %.3f ns",fg.GetParameter(1), fg.GetParError(1)),0.8); // FIXME .6
  myText(pos_x, 0.68,kBlack,Form("#sigma: %.3f #pm %.3f ns",fg.GetParameter(2),fg.GetParError(2)),0.8);

  return h_cell;


}



//
// Draw TLatex
//________________
void myText(Double_t x,Double_t y,Color_t color,char *text, double scale){
  Double_t tsize=0.05;
  tsize = tsize*scale;
  TLatex l;
  l.SetTextSize(tsize);
  l.SetNDC();
  l.SetTextColor(color);
  l.DrawLatex(x,y,text);
}


//
// ATLAS Style
//_______________
TStyle setStyle(){
  TStyle s("s","My Style");
 
   // use plain black on white colors
  Int_t icol=0; // WHITE
  s.SetFrameBorderMode(icol);
  s.SetFrameFillColor(icol);
  s.SetCanvasBorderMode(icol);
  s.SetCanvasColor(icol);
  s.SetPadBorderMode(icol);
  s.SetPadColor(icol);
  s.SetStatColor(icol);

  // set the paper & margin sizes
  s.SetPaperSize(20,26);

  // set margin sizes
  s.SetPadTopMargin(0.05);
  s.SetPadRightMargin(0.05);
  s.SetPadBottomMargin(0.16);
  s.SetPadLeftMargin(0.16);

  // set title offsets (for axis label)
  s.SetTitleXOffset(1.4);
  s.SetTitleYOffset(1.4);
  s.SetTitleOffset(1.4);

  // use large fonts
  Int_t font=42; // Helvetica
  Double_t tsize=0.05;
  s.SetTextFont(font);

  s.SetTextSize(tsize);
  s.SetLabelFont(font,"x");
  s.SetTitleFont(font,"x");
  s.SetLabelFont(font,"y");
  s.SetTitleFont(font,"y");
  s.SetLabelFont(font,"z");
  s.SetTitleFont(font,"z");

  s.SetLabelSize(tsize,"x");
  s.SetTitleSize(tsize,"x");
  s.SetLabelSize(tsize,"y");
  s.SetTitleSize(tsize,"y");
  s.SetLabelSize(tsize,"z");
  s.SetTitleSize(tsize,"z");
  
  // use bold lines and markers
  s.SetMarkerStyle(20);
  s.SetMarkerSize(1.2);
  s.SetHistLineWidth(2.);
  s.SetLineStyleString(2,"[12 12]"); // postscript dashes

  // get rid of error bar caps
  s.SetEndErrorSize(0.);

  // do not display any of the standard histogram decorations
  s.SetOptTitle(0);
  s.SetOptStat(0);
  s.SetOptFit(0);

  // put tick marks on top and RHS of plots
  s.SetPadTickX(1);
  s.SetPadTickY(1);
  
  return s;
}
