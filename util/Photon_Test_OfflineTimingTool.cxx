//___________________________________________
//
// Example macro to test Offline Timing Tool
//
//  Author: Ryne Carbone
//  Date:   Jan 2016
//  Email:  rc2809@columbia.edu
//          ryne.carbone@cern.ch
//
// Updates since Oct 2016:   Jue Chen
// Email:         jue@cern.ch
//  Information: Timing Tool only works for
//     High/Medium gain photons/photons.
//     Only cells in layer 2 have corrections
//     available, calibrated assuming energy
//     is above 5 GeV
//
//  User Input:
//    1. Make an object
//       $ OfflineTimingTool m_ott;
//    2. Find the Run Number
//    3. Find the Z Pos of the Primary Vertex
//    4. Send the el/ph, rn and PV_z
//       $ double t_cor = m_ott.getCorrectedTime<const xAOD::photon>(*el, rn, PV_z);
//
//  This Example:
//    -> This example will read from an xAOD and
//       loop throught the events. It finds the
//       run number and PV_Z, then sends both
//       with an photon to the Offline Timing
//       Tool to get a corrected time value.
//    -> The cuts on eta/gain/energy are done
//       before calling the tool for a correction
//    -> You will want to use your calibrated
//       containers if you choose a different 
//       xAOD file to run on
//    -> There are a few histograms produced
//       to illustrate the effect of the
//       corrections (out_sampleHist.root)
//
//___________________________________________

// General/ROOT includes
#include <TSystem.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TCanvas.h> 
#include <TStyle.h>
#include <TProfile.h>
#include <TLatex.h>
#include <iostream>

// Include Tool
#include "OfflineTimingTool/OfflineTimingTool.h"

// Root Access Includes
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/tools/Message.h"

// Event Info Includes
#include "xAODEventInfo/EventInfo.h"

// Egamma Includes
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/PhotonAuxContainer.h"

// Vertex Includes
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/VertexAuxContainer.h"
#include "xAODTracking/Vertex.h"

#include "TrigAnalysisInterfaces/IBunchCrossingTool.h"   
#include "TrigBunchCrossingTool/WebBunchCrossingTool.h"   

// Make summary canvas
void makeSummary(TH2F* h1, TH2F* h2, std::string sname, std::string lname);
void makeSummary(TH1F* h1, TH1F* h2, std::string sname, std::string lname);
TH2F rebin      (TH2F *h, int entPerBin, int firstBin);
// Make plots look nicer
TStyle setStyle();
void myText(Double_t x, Double_t y, Color_t color, char *text, double scale);



//
// Loop over sample xAOD
// Apply correction to time
// Plot variables before/after
// _______________________________
int main( int argc, char* argv[] ) {

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  if( argc > 1 ) submitDir = argv[ 1 ];

  // Set the style
  static TStyle myStyle = setStyle();
  myStyle.cd();

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Open an example xAOD
//  const std::string fileName = PathResolverFindCalibFile("/data/users/jchen/wtiming/ArcondCalib/IOV1_Z_slim/RootOutputs/data16_13TeV.00303846.physics_Main.merge.DAOD_EGAM1.f716_m1620_p2689.root");
 // const std::string fileName = PathResolverFindCalibFile("/afs/cern.ch/work/r/rcarbone/public/OfflineTimingTool/sample_xAOD.root");
  const std::string fileName = PathResolverFindCalibFile("/afs/cern.ch/work/j/jue/public/Offlinetool_sample/DAOD_HIGG1D1.10322014._000385.pool.root.1");
  std::unique_ptr< TFile > f( TFile::Open( fileName.data(), "READ" ) );
  
  // Make Sure File Opened Correctly
  if( f.get() ) Info( "LocalTest_OfflineTimingTool", Form("Opened sample xAOD successfully") );
  else{ Info( "LocalTest_OfflineTimingTool", Form("Failed to open sample xAOD") );return -1; }
 
  // Create output File
  TFile* outFile = new TFile("out_sampleHist.root","RECREATE");

  // Create histograms to look at results
  TH1F* h_PV_z   = new TH1F("PV_z","Primary Vertex Z position; PV_z[mm];Events / 6mm",50, -150,150);
  TH1F* h_t_in   = new TH1F("t_in","Initial Time; Initial Time[ns]; Events / 0.2 ns",100,-5,5);
  TH1F* h_t_out  = new TH1F("t_out","Final Time; Final Time[ns]; Events / 0.2 ns",100,-5,5);
  TH2F* h_e_ti   = new TH2F("e_ti","Energy vs Initial Time; Energy [GeV];Initial Time [ns]", 100, 0, 250, 100, -5, 5);
  TH2F* h_e_tf   = new TH2F("e_tf","Energy vs Final Time; Energy[GeV]; Final Time[ns]", 100, 0, 250, 100, -5, 5);
  TH2F* h_phi_ti = new TH2F("phi_ti","Phi vs Initial Time; Phi;Initial Time [ns]", 32, -3.1415, 3.1415, 100, -5, 5);
  TH2F* h_phi_tf = new TH2F("phi_tf","Phi vs Final Time; Phi; Final Time[ns]", 32, -3.1415, 3.1415, 100, -5, 5);
  TH2F* h_eta_ti = new TH2F("eta_ti","Eta vs Initial Time; Eta;Initial Time [ns]", 50, -2.5, 2.5, 100, -5, 5);
  TH2F* h_eta_tf = new TH2F("eta_tf","Eta vs Final Time; Eta; Final Time[ns]", 50, -2.5, 2.5, 100, -5, 5);
  TH2F* h_f1_ti  = new TH2F("f1_ti","Fraction Energy (1st layer) vs Initial Time; f1;Initial Time [ns]", 50, 0., 0.6, 100, -5, 5);
  TH2F* h_f1_tf  = new TH2F("f1_tf","Fraction Energy (1st layer) vs Final Time; f1; Final Time[ns]", 50, 0., 0.6, 100, -5, 5);
  TH2F* h_f3_ti  = new TH2F("f3_ti","Fraction Energy (3rd layer) vs Initial Time; f3;Initial Time [ns]", 50, 0., 0.2, 100, -5, 5);
  TH2F* h_f3_tf  = new TH2F("f3_tf","Fraction Energy (3rd layer) vs Final Time; f3; Final Time[ns]", 50, 0., 0.2, 100, -5, 5);

  // Create TEvent
  xAOD::TEvent event( static_cast<TFile*>(nullptr), xAOD::TEvent::kClassAccess);
  if( event.readFrom(f.get()) != xAOD::TReturnCode::kSuccess ){
    Info( "LocalTest_OfflineTimingTool","Failed to read TEvent!"); 
    return -1;
  }
  Trig::IBunchCrossingTool* m_bcTool; //!
  Trig::WebBunchCrossingTool *bcTool= new Trig::WebBunchCrossingTool("WebBunchCrossingTool"); 
  bcTool ->setProperty("OutputLevel", MSG::INFO);       
  bcTool ->setProperty("ServerAddress", "atlas-trigconf.cern.ch");  
  m_bcTool = bcTool;    
  m_bcTool->initialize(); 
  
  
  // Find max number of entries to loop on
  Info( "LocalTest_OfflineTimingTool", "Number of events in the file: %llu", event.getEntries() );
  Long64_t nentries = event.getEntries();
 

  // Create Offline Timing Tool object to test (optionally send debug option)
  bool debug = false;
  //nentries = 1;
  OfflineTimingTool ott(debug);
  //OfflineTimingTool ott;
  
  // Loop over the events
  for( Long64_t entry = 0; entry < nentries; ++entry){
    
    // Get the correct entry
    event.getEntry(entry);
    if( !(entry%1000) ) 
      Info( "LocalTest_OfflineTimingTool", Form("Event: %llu",entry) );

    // Get Event Info
    const xAOD::EventInfo* eventInfo = 0;
    if( event.retrieve( eventInfo, "EventInfo" ) != xAOD::TReturnCode::kSuccess ){
      Info( "LocalTest_OfflineTimingTool","Failed to retreive EventInfo Container!"); 
      return -1;
    }
    
    // Get PV_z
    const xAOD::VertexContainer* primVtxCont = 0;
    if( event.retrieve( primVtxCont, "PrimaryVertices") != xAOD::TReturnCode::kSuccess){
      Info( "LocalTest_OfflineTimingTool","Failed to retreive PrimaryVertices Container!"); 
      return -1;
    }
    xAOD::VertexContainer::const_iterator ver_itr = primVtxCont->begin();
    xAOD::VertexContainer::const_iterator ver_end = primVtxCont->end();
    
    // Get photons
    const xAOD::PhotonContainer* photons = 0;
    if( event.retrieve( photons, "Photons" ) != xAOD::TReturnCode::kSuccess){
      Info( "LocalTest_OfflineTimingTool","Failed to retreive photon Container!"); 
      return -1;
    }
  
    // Store run number 
    unsigned int rN =  eventInfo->auxdata<unsigned int>("runNumber");    
    // Store the bcid 
    unsigned int t_bcid = eventInfo->auxdata<unsigned int>("bcid"); 
    int t_dis_from_front;
    if(m_bcTool->gapBeforeTrain( t_bcid )>500)   
	     t_dis_from_front=m_bcTool->distanceFromFront( t_bcid );  
    else   
	    t_dis_from_front=m_bcTool->distanceFromFront( t_bcid)*2+m_bcTool->distanceFromTail( t_bcid)+m_bcTool->gapBeforeTrain( t_bcid);       

    
    // Store the PV data
    float PV_z = -9999;    
    for ( ; ver_itr != ver_end; ver_itr++){
      if( (*ver_itr)->vertexType() != xAOD::VxType::PriVtx) continue; // Find the PV
      if( (*ver_itr)->nTrackParticles() < 3) continue; // need 3 associated tracks
      PV_z = (*ver_itr)->z();
    }
    
    // Make sure we get a valid PV_z
    if( PV_z != -9999 ) h_PV_z->Fill( PV_z );
    else continue;
    // Double check info we will send to tool
    if( !(entry%1000) ) 
      Info( "LocalTest_OfflineTimingTool", Form(" > Run: %u  PV_z: %f  bcid: %d", rN, PV_z, t_dis_from_front) );


    
    // Loop over photons
    for( const xAOD::Photon *el : *photons ){
      
      // Only layer 2 photons!
      double eta = el->caloCluster()->eta();
      if( fabs(eta) > 2.47 || (fabs(eta) > 1.37 && fabs(eta) < 1.52) )
        continue;
      
      // At least 5 GeV!
      double el_e = el->auxdata<float>("maxEcell_energy")/1000.;
      if( el_e < 5. )
        continue;
      
      // Only High/Med Gain
      int el_gain = el->auxdata<int>("maxEcell_gain");
   //   if( el_gain < 0 || el_gain > 1)
   //     continue;
      
      // Fill some initial plots
      float el_eta = el->caloCluster()->eta();
      float el_phi = el->caloCluster()->phi();
      float el_f1  = el->auxdata<float>("f1");
      float el_f3  = el->auxdata<float>("f3");
      float el_t = el->auxdata<float>("maxEcell_time");
      h_t_in->Fill(el_t);
      h_e_ti->Fill(el_e,el_t);
      h_phi_ti->Fill(el_phi,el_t);
      h_eta_ti->Fill(el_eta,el_t);
      h_f1_ti->Fill(el_f1,el_t);
      h_f3_ti->Fill(el_f3,el_t);
     
      bool t_valid;
      double el_t_corr;
      // Get the corrected time
      tie(t_valid, el_t_corr )= ott.getCorrectedTime<const xAOD::Photon>(*el, rN, PV_z, t_dis_from_front );

      
      // If time received is actually diff, fill final plots
      if( el_t != el_t_corr ){
        h_t_out->Fill(el_t_corr);
        h_e_tf->Fill(el_e,el_t_corr);
        h_phi_tf->Fill(el_phi,el_t_corr);
        h_eta_tf->Fill(el_eta,el_t_corr);
        h_f1_tf->Fill(el_f1,el_t_corr);
        h_f3_tf->Fill(el_f3,el_t_corr);
      }
      // Print initial/final time
      if( !(entry%1000) ) 
        Info("LocalTest_OfflineTimingTool", Form("     Initial time: %f;\tFinal time: %f", el_t, el_t_corr) );
      if( !t_valid)
	      Info("LocalTest_OfflineTimingTool", Form("     Initial time: %f;\tFinal time: %f; valid: %d", el_t, el_t_corr, t_valid) );
    }// end loop over photons

  }// end loop over events
 
  // Rebin energy, f1, f3
  (*h_e_ti)  = rebin( h_e_ti, h_e_ti->GetEntries()/20, 2);
  (*h_e_tf)  = rebin( h_e_tf, h_e_tf->GetEntries()/20, 2);
  (*h_f1_ti) = rebin( h_f1_ti, h_f1_ti->GetEntries()/20,  h_f1_ti->GetXaxis()->FindBin(0.)-1);
  (*h_f1_tf) = rebin( h_f1_tf, h_f1_tf->GetEntries()/20,  h_f1_tf->GetXaxis()->FindBin(0.)-1);
  (*h_f3_ti) = rebin( h_f3_ti, h_f3_ti->GetEntries()/20,  h_f3_ti->GetXaxis()->FindBin(0.)-1);
  (*h_f3_tf) = rebin( h_f3_tf, h_f3_tf->GetEntries()/20,  h_f3_tf->GetXaxis()->FindBin(0.)-1);

  // Make summary plots
  makeSummary(h_t_in, h_t_out, "time", "Time Before & After corrections");
  makeSummary(h_e_ti, h_e_tf, "energy", "Energy vs Time Before & After Corrections");
  makeSummary(h_phi_ti, h_phi_tf, "phi", "Phi vs Time Before & After Corrections");
  makeSummary(h_eta_ti, h_eta_tf, "eta", "Eta vs Time Before & After Corrections");
  makeSummary(h_f1_ti, h_f1_tf, "f1", "f1 vs Time Before & After Corrections");
  makeSummary(h_f3_ti, h_f3_tf, "f3", "f3 vs Time Before & After Corrections");

  // Save histograms to file 
  outFile->Write();
  Info("LocalTest_OfflineTimingTool","Saved summary histograms to out_sampleHist.root");
  
  // Close File
  outFile->Close();

  return 0;
}


//
// Take 2D hist before/after time correction
// Compare distributions, plot mean/rms
//_______________________________________________________________________
void makeSummary(TH2F* hi, TH2F* hf, std::string sname, std::string lname){
  
  // TProfile Before/After
  TCanvas* c1 = new TCanvas(Form("c_%s",sname.data()), Form("%s",lname.data()), 800,700);
  TProfile *tp1 = hi->ProfileX();
  TProfile *tp2 = hf->ProfileX();
  tp1->SetTitle(lname.data()); // Put title on Plot
  tp1->GetYaxis()->SetRangeUser(-2.5,2.5);
  tp1->GetYaxis()->SetTitle("Time [ns]");
  tp1->Draw();
  tp2->SetLineColor(kRed);
  tp2->SetMarkerColor(kRed);
  tp2->Draw("same");
  if( sname == "f3" || sname == "energy") c1->SetLogx(1);
  
  // Put Mean/RMS time on plot
  myText(0.20, 0.88, kBlack, Form("Data"), 1.0);
  myText(0.65, 0.85, kBlack, Form("t(initial) Mean: %.2f ns",hi->GetMean(2)), 0.6 );
  myText(0.65, 0.80, kBlack, Form("t(initial) RMS:  %.2f ns",hi->GetRMS(2)), 0.6 );
  myText(0.65, 0.75, kRed,   Form("t(final) Mean: %.2f ns",hf->GetMean(2)), 0.6 );
  myText(0.65, 0.70, kRed,   Form("t(final) RMS:  %.2f ns",hf->GetRMS(2)), 0.6 );
 
  c1->Write();
  
  delete tp1;
  delete tp2;
  delete c1;
}


//
// Overloaded to accept TH1F
//______________________________________________________________________
void makeSummary(TH1F* hi, TH1F* hf, std::string sname, std::string lname){
  
  // Before/After
  TCanvas* c1 = new TCanvas(Form("c_%s",sname.data()), Form("%s",lname.data()), 800,700);
  hi->SetTitle(lname.data());
  hi->Draw();
  hi->GetYaxis()->SetRangeUser(0.5,hf->GetMaximum()*8);
  hf->SetLineColor(kRed);
  hf->Draw("same");
  c1->SetLogy();
  
  // Put Mean/RMS time on plot
  myText(0.20, 0.88, kBlack, Form("Data"), 1.0);
  myText(0.65, 0.85, kBlack, Form("t(initial) Mean: %.2f ns",hi->GetMean()), 0.6 );
  myText(0.65, 0.80, kBlack, Form("t(initial) RMS:  %.2f ns",hi->GetRMS()), 0.6 );
  myText(0.65, 0.75, kRed,   Form("t(final) Mean: %.2f ns",hf->GetMean()), 0.6 );
  myText(0.65, 0.70, kRed,   Form("t(final) RMS:  %.2f ns",hf->GetRMS()), 0.6 );

  c1->Write();
  
  delete c1;
}

//
// Rebin the 2D histogram so that each bin has at
// least entPerBin entires
// firstBin is the first bin you want to consider
//________________________________________________
TH2F rebin(TH2F *h, int entPerBin, int firstBin) {

  // hold the bin edge information
  std::vector< double > xbins;
  // Combine all bins below first bin (no data below 5Gev so these will be empty)
  xbins.push_back( h->GetXaxis()->GetBinLowEdge(1) );
  xbins.push_back( h->GetXaxis()->GetBinLowEdge(firstBin + 1) );

  // keep track of last bin with at least entPerBin
  int lastFullIndex = firstBin;

  // Get the xaxis
  TAxis *axis = h->GetXaxis();

  // Loop over bins in xaxis, start after first bin
  for (int i = (firstBin + 1); i <= h->GetNbinsX() - firstBin; i++) {

    // Get entries in this bin, and width
    int    y = h->Integral(i,i);
    double w = axis->GetBinWidth(i);

    // If not enough entries, need to combine bins
    if (y <= entPerBin){
      // Find integral from last combined bin
      double integral = h->Integral(lastFullIndex+1, i);
      if (integral <= entPerBin ) continue;
      // if above threshold, mark as new bin
      lastFullIndex = i;
      xbins.push_back( axis->GetBinLowEdge(i) + w);
    }
    else{
      // above threshold, mark as bin
      lastFullIndex = i;
      xbins.push_back( axis->GetBinLowEdge(i) + w );
    }

  }

  // put bin edges into an array
  xbins.push_back( axis->GetXmax() );
  size_t s = xbins.size();
  double *xbinsFinal = &xbins[0];

  // create new histo with new bin edges
  TH2F h_return = TH2F( Form("hrebin_%s",h->GetName()),
                         h->GetTitle(),
                         s-1, xbinsFinal,
                         h->GetNbinsY(), -5, 5);
  h_return.GetXaxis()->SetTitle( h->GetXaxis()->GetTitle() );

  // fill new histo with old values
  for( int i=1; i<=h->GetNbinsX(); i++){
    for( int j=1; j<=h->GetNbinsY(); j++){
      h_return.Fill( h->GetXaxis()->GetBinCenter(i),
                     h->GetYaxis()->GetBinCenter(j),
                     h->GetBinContent(i,j));
    }
  }
  
  return h_return;
}



//
// Draw TLatex
//________________
void myText(Double_t x,Double_t y,Color_t color,char *text, double scale){
  Double_t tsize=0.05;
  tsize = tsize*scale;
  TLatex l;
  l.SetTextSize(tsize);
  l.SetNDC();
  l.SetTextColor(color);
  l.DrawLatex(x,y,text);
}


//
// ATLAS Style
//_______________
TStyle setStyle(){
  TStyle s("s","My Style");

   // use plain black on white colors
  Int_t icol=0; // WHITE
  s.SetFrameBorderMode(icol);
  s.SetFrameFillColor(icol);
  s.SetCanvasBorderMode(icol);
  s.SetCanvasColor(icol);
  s.SetPadBorderMode(icol);
  s.SetPadColor(icol);
  s.SetStatColor(icol);

  // set the paper & margin sizes
  s.SetPaperSize(20,26);

  // set margin sizes
  s.SetPadTopMargin(0.05);
  s.SetPadRightMargin(0.05);
  s.SetPadBottomMargin(0.16);
  s.SetPadLeftMargin(0.16);

  // set title offsets (for axis label)
  s.SetTitleXOffset(1.4);
  s.SetTitleYOffset(1.4);
  s.SetTitleOffset(1.4);

  // use large fonts
  Int_t font=42; // Helvetica
  Double_t tsize=0.05;
  s.SetTextFont(font);

  s.SetTextSize(tsize);
  s.SetLabelFont(font,"x");
  s.SetTitleFont(font,"x");
  s.SetLabelFont(font,"y");
  s.SetTitleFont(font,"y");
  s.SetLabelFont(font,"z");
  s.SetTitleFont(font,"z");

  s.SetLabelSize(tsize,"x");
  s.SetTitleSize(tsize,"x");
  s.SetLabelSize(tsize,"y");
  s.SetTitleSize(tsize,"y");
  s.SetLabelSize(tsize,"z");
  s.SetTitleSize(tsize,"z");

  // use bold lines and markers
  s.SetMarkerStyle(20);
  s.SetMarkerSize(1.2);
  s.SetHistLineWidth(2.);
  s.SetLineStyleString(2,"[12 12]"); // postscript dashes

  // get rid of error bar caps
  s.SetEndErrorSize(0.);
  // do not display any of the standard histogram decorations
  s.SetOptTitle(0);
  s.SetOptStat(0);
  s.SetOptFit(0);

  // put tick marks on top and RHS of plots
  s.SetPadTickX(1);
  s.SetPadTickY(1);


  return s;
}
